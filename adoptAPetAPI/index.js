const express = require('express');
const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser({ extended: true }));

app.get('/', (req, res) => { res.status(200).send('Olá mundo') });

app.post('/', (req, res) => {
  res.status(201).send(req.body)

});

app.listen(3000, () => {
  console.log('listening... at 3000 port.')
});



