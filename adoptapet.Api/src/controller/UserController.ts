import {User} from "../entity/User";
import { BaseController } from "./BaseController";
import { Request } from 'express';
import * as md5 from 'md5';
export class UserController extends BaseController<User> {

    constructor(){
        super(User);
    }

    async createUser(request:Request){
        let { name, photo, email, isRoot, password, confirmPassword } = request.body;
        super.isRequired(name, "Infome o nome");
        super.isRequired(photo, "Infome a foto");
        super.isRequired(email, "Infome o email");
        super.isRequired(password, "Infome o password");
        super.isRequired(confirmPassword, "Infome a confirmação do password");
        let _user = new User;
        _user.name = name;
        _user.photo = photo;
        _user.email = email;
        if (password !== confirmPassword ){
            return {status: 400, errors:['A senha e a confirmação são diferentes']}
        }
        if (password) {
            _user.password = md5(password);
        }
        _user.password = password;
        _user.isRoot = isRoot;
        return super.save(_user)
    }

    async save(request : Request) {
        let _user = request.body;
        super.isRequired(_user.name,'O nome do usuário é obrigatório');
        super.isRequired(_user.photo, 'A foto do usuário é obrigatória');
        super.isRequired(_user.email, 'O email do usuário é obrigatório');
        super.isRequired(_user.password, 'O password do usuário é obrigatório');
    }

}