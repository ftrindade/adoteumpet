import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";
import { BaseEntity } from "./BaseEntity";

@Entity()
export class User extends BaseEntity {

    @Column({type:'varchar', length:100})
    name: string;

    @Column({ type: 'varchar', length: 250 })
    photo: string;

    @Column({ type: 'varchar', length: 200 })
    email: string;

    @Column({ default: false})
    isRoot: boolean;

    @Column( {default: '', type:'varchar', length: 100, nullable: true} )
    password: string;

}
